package com.diegopizzo.githubstargazers;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.diegopizzo.githubstargazers.ui.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by diegopizzo on 01/12/2017.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureSetContentViewOfActivityWork() {
        onView(withId(R.id.activity_view_content))
                .check(matches(isDisplayed()));
    }

    @Test
    public void ensureFabIsNotVisibleAtStart() {
        onView(withId(R.id.floatingActionButton))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));
    }

    @Test
    public void ensureUserInputCardIsVisibleAtStart() {
        onView(withId(R.id.user_input_include_layout))
                .check(matches(isDisplayed()));
    }

    @Test
    public void ensureEditTextShowWordsTyped() {
        onView(withId(R.id.ownerEditText)).perform(typeText("HELLO"));
        onView(withId(R.id.repoEditText)).perform(typeText("WORLD"));
        onView(withId(R.id.ownerEditText)).check(matches(withText("HELLO")));
        onView(withId(R.id.repoEditText)).check(matches(withText("WORLD")));
    }

    @Test
    public void ensureClearButtonSetEmptyStringInsideTheEditTextsWhenClicked() {
        onView(withId(R.id.ownerEditText)).perform(typeText("GitHub"));
        onView(withId(R.id.repoEditText)).perform(typeText("Stargazers"));
        onView(withId(R.id.clearButton)).perform(click());
        onView(withId(R.id.ownerEditText)).check(matches(withText("")));
        onView(withId(R.id.repoEditText)).check(matches(withText("")));
    }

    private void typeOwnerAndRepoForNotFindStargazers() {
        onView(withId(R.id.ownerEditText)).perform(typeText("hjgjhghjgghjghjjgh"));
        onView(withId(R.id.repoEditText)).perform(typeText("hkjhkjhkjhkjhkj"));
    }

    private void typeOwnerAndRepoForFindStargazers() {
        onView(withId(R.id.ownerEditText)).perform(typeText("gemgon"));
        onView(withId(R.id.repoEditText)).perform(typeText("octocat"));
    }

    @Test
    public void ensureUserInputCardIsNotHiddenWhenNotFoundStargazers() {
        typeOwnerAndRepoForNotFindStargazers();
        onView(withId(R.id.searchButton)).perform(click());
        onView(withId(R.id.user_input_include_layout))
                .check(matches(isDisplayed()));
    }

    @Test
    public void ensureUserInputCardIsHiddenWhenFoundStargazers() {
        typeOwnerAndRepoForFindStargazers();
        onView(withId(R.id.searchButton)).perform(click());
        onView(withId(R.id.user_input_include_layout))
                .check(matches(not(isDisplayed())));
    }

    @Test
    public void ensureFabIsNotVisibleWhenNotFoundStargazers() {
        typeOwnerAndRepoForNotFindStargazers();
        onView(withId(R.id.searchButton)).perform(click());
        onView(withId(R.id.floatingActionButton))
                .check(matches(not(isDisplayed())));
    }

    @Test
    public void ensureFabIsVisibleWhenFoundStargazers() {
        typeOwnerAndRepoForFindStargazers();
        onView(withId(R.id.searchButton)).perform(click());
        onView(withId(R.id.floatingActionButton))
                .check(matches(isDisplayed()));
    }

    @Test
    public void ensureFabSetVisibleUserCardViewWhenClicked() {
        onView(withId(R.id.floatingActionButton))
                .check(matches(not(isDisplayed())));
        typeOwnerAndRepoForFindStargazers();
        onView(withId(R.id.searchButton)).perform(click());
        onView(withId(R.id.floatingActionButton))
                .check(matches(isDisplayed()));
    }
}
