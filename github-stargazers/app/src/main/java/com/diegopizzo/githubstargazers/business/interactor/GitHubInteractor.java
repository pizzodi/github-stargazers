package com.diegopizzo.githubstargazers.business.interactor;

import com.diegopizzo.githubstargazers.business.network.cache.GitHubUserStore;
import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public class GitHubInteractor {

    private final GitHubUserStore gitHubUserStore;

    public GitHubInteractor(final GitHubUserStore gitHubUserStore) {
        this.gitHubUserStore = gitHubUserStore;
    }

    public Single<List<GitHubUser>> getUsersStargazers(final String owner, final String repo, final int page) {
        return gitHubUserStore.storeData(owner + ";" + repo + ";" + page);
    }
}
