package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import com.diegopizzo.githubstargazers.business.interactor.GitHubInteractor;
import com.diegopizzo.githubstargazers.config.dagger.FragmentScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@Module
class StargazersFragmentModule {

    private final StargazersFragmentContract.View view;

    StargazersFragmentModule(final StargazersFragmentContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    StargazersFragmentContract.Presenter providePresenter(final GitHubInteractor gitHubInteractor) {
        return new StargazersFragmentPresenter(view, gitHubInteractor);
    }
}
