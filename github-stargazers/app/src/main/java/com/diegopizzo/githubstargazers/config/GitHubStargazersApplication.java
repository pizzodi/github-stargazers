package com.diegopizzo.githubstargazers.config;

import android.app.Application;

import com.diegopizzo.githubstargazers.config.dagger.ApplicationComponent;
import com.diegopizzo.githubstargazers.config.dagger.ApplicationModule;
import com.diegopizzo.githubstargazers.config.dagger.DaggerApplicationComponent;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public class GitHubStargazersApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
