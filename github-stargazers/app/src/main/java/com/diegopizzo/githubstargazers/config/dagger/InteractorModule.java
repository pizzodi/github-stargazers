package com.diegopizzo.githubstargazers.config.dagger;

import com.diegopizzo.githubstargazers.business.interactor.GitHubInteractor;
import com.diegopizzo.githubstargazers.business.network.cache.GitHubUserStore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@Module
class InteractorModule {

    @Provides
    @Singleton
    GitHubInteractor providesGitHubServiceInteractor(final GitHubUserStore gitHubUserStore) {
        return new GitHubInteractor(gitHubUserStore);
    }

}
