package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import android.util.Log;

import com.diegopizzo.githubstargazers.business.interactor.GitHubInteractor;
import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public class StargazersFragmentPresenter implements StargazersFragmentContract.Presenter {

    private static final String STARGAZERS_NOT_FOUND = "This repository has no stargazers";
    private static final String STARGAZERS_FOUND = "Stargazers found";
    private static final String NOT_EXISTS = "Owner or Repo not exists";
    private static final String API_LIMIT = "API limit exceeded";
    private static final String ERROR = "Somenthing went wrong!";
    private final StargazersFragmentContract.View view;
    private final GitHubInteractor gitHubInteractor;

    StargazersFragmentPresenter(final StargazersFragmentContract.View view, final GitHubInteractor gitHubInteractor) {
        this.view = view;
        this.gitHubInteractor = gitHubInteractor;
    }

    @Override
    public void getStargazersUsers(final String owner, final String repo, final int page) {

        final Single<List<GitHubUser>> gitHubUsersObservable = gitHubInteractor.getUsersStargazers(owner, repo, page);

        final Disposable disposable = gitHubUsersObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).doOnSubscribe(disposable1 -> view.showLoading())
                .doFinally(view::showContent)
                .subscribe(gitHubUsers -> {
                    if (gitHubUsers != null && !gitHubUsers.isEmpty()) {
                        if (page == 1) {
                            view.setDataOnRecyclerViewFirstPage(gitHubUsers);
                        } else {
                            view.setDataOnRecyclerViewNextPages(gitHubUsers);
                        }
                        view.hideUserInputView();
                        Log.i("Success", STARGAZERS_FOUND);
                    } else {
                        if (page == 1) {
                            view.showMessage(STARGAZERS_NOT_FOUND);
                            Log.i("Empty", STARGAZERS_NOT_FOUND);
                        }
                    }
                }, (Throwable throwable) -> {
                    view.showContent();
                    getMessage(throwable);
                });

    }

    @Override
    public void resumeGitHubUserList(final String owner, final String repo, final int pages) {
        for (int i = 1; i <= pages; i++) getStargazersUsers(owner, repo, i);
    }


    private void getMessage(final Throwable throwable) {
        if (throwable instanceof HttpException) {
            final HttpException httpException = (HttpException) throwable;
            switch (httpException.code()) {
                case 404:
                    view.showMessage(NOT_EXISTS);
                    Log.i("Empty", NOT_EXISTS);
                    break;
                case 403:
                    view.showMessage(API_LIMIT);
                    Log.i("ApiLimit", API_LIMIT);
                    break;
                default:
                    view.showMessage(ERROR);
                    Log.e("error", throwable.getMessage());
            }
        }
    }
}
