package com.diegopizzo.githubstargazers.config.dagger;

import com.diegopizzo.githubstargazers.business.interactor.GitHubInteractor;
import com.diegopizzo.githubstargazers.business.network.cache.GitHubUserStore;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class, InteractorModule.class, NetworkModule.class, GitHubUserStoreModule.class})
public interface ApplicationComponent {

    GitHubInteractor providesGitHubServiceInteractor();

    GitHubUserStore providesGitHubUserStore();
}
