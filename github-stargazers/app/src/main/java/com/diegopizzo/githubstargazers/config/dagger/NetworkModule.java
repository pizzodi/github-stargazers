package com.diegopizzo.githubstargazers.config.dagger;

import com.diegopizzo.githubstargazers.business.network.service.GitHubService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@Module
class NetworkModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(final GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(GitHubService.SERVICE_ENDPOINT)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory(final Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().serializeNulls().create();
    }


    @Provides
    @Singleton
    GitHubService provideGitHubService(final Retrofit retrofit) {
        return retrofit.create(GitHubService.class);
    }
}
