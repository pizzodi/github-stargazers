package com.diegopizzo.githubstargazers.config.dagger;

import android.content.Context;

import dagger.Module;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(final Context context) {
        this.context = context;
    }
}
