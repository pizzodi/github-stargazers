package com.diegopizzo.githubstargazers.business.network.cache;

import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;
import com.diegopizzo.githubstargazers.business.network.service.GitHubService;
import com.nytimes.android.external.store3.base.impl.Store;
import com.nytimes.android.external.store3.base.impl.StoreBuilder;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by diegopizzo on 02/12/2017.
 */

public class GitHubUserStore {

    private final GitHubService gitHubService;
    private final Store<List<GitHubUser>, String> store;

    public GitHubUserStore(final GitHubService gitHubService) {
        this.gitHubService = gitHubService;

        store = StoreBuilder.<String, List<GitHubUser>>key()
                .fetcher(key ->
                        gitHubService.getUsersStargazers(key.split(";")[0], key.split(";")[1], Integer.valueOf(key.split(";")[2])))
                .open();
    }

    public Single<List<GitHubUser>> storeData(final String key) {
        return store.get(key);
    }
}
