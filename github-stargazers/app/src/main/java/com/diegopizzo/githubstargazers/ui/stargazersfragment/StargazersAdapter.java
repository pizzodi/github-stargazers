package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.diegopizzo.githubstargazers.R;
import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public class StargazersAdapter extends RecyclerView.Adapter<StargazersAdapter.ViewHolder> {

    private final Context mContext;
    private final List<GitHubUser> gitHubUserList;

    StargazersAdapter(final Context mContext) {
        this.mContext = mContext;
        gitHubUserList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View stargazerUserItemView = inflater.inflate(R.layout.stargazers_item, parent, false);
        return new ViewHolder(stargazerUserItemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GitHubUser gitHubUser = gitHubUserList.get(position);
        holder.user.setText(gitHubUser.getLogin());
        Glide.with(mContext).load(gitHubUser.getAvatarUrl()).into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return gitHubUserList.size();
    }

    void setItemsFirstPage(final List<GitHubUser> gitHubUserListToAdd) {
        gitHubUserList.clear();
        gitHubUserList.addAll(gitHubUserListToAdd);
        notifyDataSetChanged();
    }

    void setItemsNextPages(final List<GitHubUser> gitHubUserListToAdd) {
        final int curSize = gitHubUserList.size();
        gitHubUserList.addAll(gitHubUserListToAdd);
        notifyItemRangeChanged(curSize, gitHubUserList.size() - 1);
    }

    void clearRecyclerView() {
        gitHubUserList.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView avatar;
        TextView user;

        ViewHolder(final View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatarImageView);
            user = itemView.findViewById(R.id.usernameTextView);
        }
    }
}
