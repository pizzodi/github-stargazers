package com.diegopizzo.githubstargazers.business.network.service;

import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public interface GitHubService {

    String SERVICE_ENDPOINT = "https://api.github.com";

    @Headers("Content-Type: application/json")
    @GET("/repos/{owner}/{repo}/stargazers")
    Single<List<GitHubUser>> getUsersStargazers(@Path("owner") String owner, @Path("repo") String repo, @Query("page") int page);
}
