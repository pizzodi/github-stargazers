package com.diegopizzo.githubstargazers.config.dagger;

import com.diegopizzo.githubstargazers.business.network.cache.GitHubUserStore;
import com.diegopizzo.githubstargazers.business.network.service.GitHubService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegopizzo on 02/12/2017.
 */

@Module
public class GitHubUserStoreModule {
    @Provides
    @Singleton
    GitHubUserStore providesGitHubUserStore(final GitHubService gitHubService) {
        return new GitHubUserStore(gitHubService);
    }
}
