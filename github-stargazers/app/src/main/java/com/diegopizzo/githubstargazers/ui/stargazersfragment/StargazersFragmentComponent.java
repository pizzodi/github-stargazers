package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import com.diegopizzo.githubstargazers.config.dagger.ApplicationComponent;
import com.diegopizzo.githubstargazers.config.dagger.FragmentScope;

import dagger.Component;

/**
 * Created by diegopizzo on 30/11/2017.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = StargazersFragmentModule.class)
public interface StargazersFragmentComponent {

    void inject(StargazersFragment stargazersFragment);
}
