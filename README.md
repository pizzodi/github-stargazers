# GitHub Stargazers

Questo progetto è una semplice applicazione Android che utilizzando le API di GitHub, mostra la lista dei stargazers di un repository. Dando come input l'owner e il nome del repository verrà visualizzata,  se presente,  una lista formata dall'avatar e dall'username dell'utente che ha messo tra i preferiti quel determinato repository.

Il progetto è stato creato utilizzando Android Studio 3.0 e con l'ausilio di librerie di terze parti open source.

**L'app è formata da due View:**
- un Card View dove è possibile inserire l'owner e il repository
- una Recycler View dove vengono mostrati gli stargazers

# Architettura
Questo progetto è basato sul pattern architetturale MVP.

# Librerie di terze parti
**Dagger2**  
Dagger è un framework per la dependency injection.
Link: https://google.github.io/dagger/

**Butter Knife**  
Utilizzato per il binding dei campi.
Link: https://jakewharton.github.io/butterknife/

**Retrofit**  
HTTP client per Android.
Link: https://square.github.io/retrofit/

**Glide**  
un potente image loader per Android.
Link: https://github.com/bumptech/glide

**NYTimes Store**  
libreria che implementa il repository pattern, utilizzata per il memory caching. 
Link: https://github.com/NYTimes/Store

**RxJava/RxAndroid**  
libreria per costruire chiamate asincrone. 
Link: https://github.com/ReactiveX/RxJava
Link: https://github.com/ReactiveX/RxAndroid

**AwesomeValidation**
libreria per la validazione dei dati.
Link: https://github.com/thyrlian/AwesomeValidation